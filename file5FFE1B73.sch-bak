EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 2 4
Title "Shared Ground Array Controller"
Date "2021-01-12"
Rev "A"
Comp "Ensemble Solutions, LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_Coaxial J?
U 1 1 60009F69
P 2225 1225
F 0 "J?" H 2153 1463 50  0000 C CNN
F 1 "Conn_Coaxial" H 2153 1372 50  0000 C CNN
F 2 "_projectFootprints:Conn_FType_Coax_BoardMount" H 2225 1225 50  0001 C CNN
F 3 " ~" H 2225 1225 50  0001 C CNN
	1    2225 1225
	-1   0    0    -1  
$EndComp
$Comp
L _projectSymbols:CAPACITOR_CHIP_NON-POLARIZED_EIA_0603-METRIC_1608X95 C?
U 1 1 60009F70
P 2725 1325
F 0 "C?" H 2853 1266 50  0000 L CNN
F 1 "1uF" H 2853 1175 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2925 875 50  0001 C CNN
F 3 "" H 2765 1250 50  0001 C CNN
F 4 "V?" H 2853 1084 50  0000 L CNN "Voltage"
	1    2725 1325
	1    0    0    -1  
$EndComp
$Comp
L _projectSymbols:XFMR_DRQ_eaton T?
U 1 1 60009F77
P 2825 1825
F 0 "T?" V 3179 1953 50  0000 L CNN
F 1 "DRQ127470" H 3025 1575 50  0000 L CNN
F 2 "_projectFootprints:DRQ127470R" H 3475 1925 50  0001 L CNN
F 3 "https://www.digikey.com/en/products/detail/eaton-electronics-division/DRQ127-470-R/667269?s=N4IgTCBcDaICICUCKBGMB2AtAFnQBkwRAF0BfIA" H 3475 1825 50  0001 L CNN
F 4 "Eaton DRQ Series" H 3475 1725 50  0001 L CNN "Description"
	1    2825 1825
	0    1    1    0   
$EndComp
$Comp
L _projectSymbols:XFMR_DRQ_eaton T?
U 1 1 60009F7E
P 5075 1825
F 0 "T?" V 5429 1953 50  0000 L CNN
F 1 "DRQ127470" H 5275 1575 50  0000 L CNN
F 2 "_projectFootprints:DRQ127470R" H 5725 1925 50  0001 L CNN
F 3 "https://www.digikey.com/en/products/detail/eaton-electronics-division/DRQ127-470-R/667269?s=N4IgTCBcDaICICUCKBGMB2AtAFnQBkwRAF0BfIA" H 5725 1825 50  0001 L CNN
F 4 "Eaton DRQ Series" H 5725 1725 50  0001 L CNN "Description"
	1    5075 1825
	0    1    1    0   
$EndComp
$Comp
L _projectSymbols:DR331-105BE T?
U 1 1 60009F8E
P 3475 2175
F 0 "T?" H 3875 2440 50  0000 C CNN
F 1 "4700uH" H 3875 2349 50  0000 C CNN
F 2 "DR331105BE" H 4125 2275 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/8749687" H 4125 2175 50  0001 L CNN
F 4 "Bourns DR331 Series SMD Common Mode Choke with a Ferrite Core, 1 mH Wire-Wound 500mA Idc" H 4125 2075 50  0001 L CNN "Description"
F 5 "5.3" H 4125 1975 50  0001 L CNN "Height"
F 6 "Bourns" H 4125 1875 50  0001 L CNN "Manufacturer_Name"
F 7 "DR331-105BE" H 4125 1775 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-DR331-105BE" H 4125 1675 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Bourns/DR331-105BE?qs=CrTVAl0CcnKk6%252BoOsQlt3w%3D%3D" H 4125 1575 50  0001 L CNN "Mouser Price/Stock"
F 10 "8749687P" H 4125 1475 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/8749687P" H 4125 1375 50  0001 L CNN "RS Price/Stock"
F 12 "71279285" H 4125 1275 50  0001 L CNN "Allied_Number"
F 13 "https://www.alliedelec.com/bourns-dr331-105be/71279285/" H 4125 1175 50  0001 L CNN "Allied Price/Stock"
	1    3475 2175
	1    0    0    -1  
$EndComp
Wire Wire Line
	2225 1425 2225 2775
Wire Wire Line
	2225 2775 2475 2775
Wire Wire Line
	2725 2775 2725 2625
Wire Wire Line
	2725 1825 2725 1625
Wire Wire Line
	2725 1325 2725 1225
Wire Wire Line
	2725 1225 2425 1225
Wire Wire Line
	2825 1825 2825 1725
Wire Wire Line
	3325 2175 3475 2175
Wire Wire Line
	2825 2625 2825 2775
Wire Wire Line
	2825 2775 3325 2775
Wire Wire Line
	3325 2775 3325 2275
Wire Wire Line
	3325 2275 3475 2275
Wire Wire Line
	4575 2175 4575 1275
Wire Wire Line
	4575 1275 5075 1275
Wire Wire Line
	5075 1275 5075 1825
Wire Wire Line
	4275 2175 4575 2175
Wire Wire Line
	4975 2625 4975 2775
Wire Wire Line
	4975 2775 4575 2775
Wire Wire Line
	4575 2775 4575 2275
Wire Wire Line
	4575 2275 4275 2275
Wire Wire Line
	2725 1225 3325 1225
Connection ~ 2725 1225
Wire Wire Line
	3325 1225 3325 2175
Wire Wire Line
	2825 1725 2475 1725
Wire Wire Line
	2475 1725 2475 2775
Connection ~ 2475 2775
Wire Wire Line
	2475 2775 2725 2775
Wire Wire Line
	4975 1825 4975 1725
Wire Wire Line
	4975 1725 5525 1725
$Comp
L _projectSymbols:CAPACITOR_CHIP_NON-POLARIZED_EIA_0603-METRIC_1608X95 C?
U 1 1 60009FB2
P 5525 2125
F 0 "C?" H 5653 2066 50  0000 L CNN
F 1 "1uF" H 5653 1975 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5725 1675 50  0001 C CNN
F 3 "" H 5565 2050 50  0001 C CNN
F 4 "V?" H 5653 1884 50  0000 L CNN "Voltage"
	1    5525 2125
	1    0    0    -1  
$EndComp
$Comp
L _projectSymbols:CAPACITOR_CHIP_NON-POLARIZED_EIA_0603-METRIC_1608X95 C?
U 1 1 60009FB9
P 6025 2125
F 0 "C?" H 6153 2066 50  0000 L CNN
F 1 "0.1uF" H 6153 1975 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6225 1675 50  0001 C CNN
F 3 "" H 6065 2050 50  0001 C CNN
F 4 "V?" H 6153 1884 50  0000 L CNN "Voltage"
	1    6025 2125
	1    0    0    -1  
$EndComp
Wire Wire Line
	5075 2625 5075 2775
Wire Wire Line
	5075 2775 5525 2775
Wire Wire Line
	5525 2125 5525 1725
Connection ~ 5525 1725
Wire Wire Line
	5525 1725 6025 1725
Wire Wire Line
	6025 2125 6025 1725
Connection ~ 6025 1725
Wire Wire Line
	6025 2425 6025 2775
Connection ~ 6025 2775
Wire Wire Line
	6025 2775 6575 2775
Wire Wire Line
	5525 2425 5525 2775
Connection ~ 5525 2775
Wire Wire Line
	5525 2775 6025 2775
Wire Wire Line
	6525 1875 6525 1725
Wire Wire Line
	6025 1725 6525 1725
Text GLabel 6575 2775 2    50   Input ~ 0
PWR_DATA_OUT
Wire Wire Line
	5075 1275 5925 1275
Connection ~ 5075 1275
$Comp
L _projectSymbols:CAPACITOR_CHIP_NON-POLARIZED_EIA_0603-METRIC_1608X95 C?
U 1 1 60009FD2
P 6225 1275
F 0 "C?" V 5826 1125 50  0000 C CNN
F 1 "1uF" V 5917 1125 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6425 825 50  0001 C CNN
F 3 "" H 6265 1200 50  0001 C CNN
F 4 "V?" V 6008 1125 50  0000 C CNN "Voltage"
	1    6225 1275
	0    1    1    0   
$EndComp
$Comp
L _projectSymbols:Transformer_Auto_Custom T?
U 1 1 60009FDA
P 8425 1875
F 0 "T?" H 8978 1934 59  0000 L CNN
F 1 "35T0375-10H" H 8978 2037 56  0000 L CNN
F 2 "_projectFootprints:XFMR_Toroid_Core_Laird_35T0375-10H" V 8875 1625 50  0001 C CNN
F 3 "" V 8875 1625 50  0001 C CNN
F 4 "2t" H 8978 2132 50  0000 L CNN "TurnsPrimary"
F 5 "5t" H 8978 2223 50  0000 L CNN "TurnsSecondary"
	1    8425 1875
	-1   0    0    1   
$EndComp
Wire Wire Line
	8025 1625 8025 1275
Wire Wire Line
	6225 1275 8025 1275
Text Notes 1925 925  0    50   ~ 0
FEEDLINE TO ARRAY
$Comp
L power:GND #PWR?
U 1 1 60009FE3
P 6525 1875
F 0 "#PWR?" H 6525 1625 50  0001 C CNN
F 1 "GND" H 6530 1702 50  0000 C CNN
F 2 "" H 6525 1875 50  0001 C CNN
F 3 "" H 6525 1875 50  0001 C CNN
	1    6525 1875
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60009FE9
P 8025 2525
F 0 "#PWR?" H 8025 2275 50  0001 C CNN
F 1 "GND" H 8030 2352 50  0000 C CNN
F 2 "" H 8025 2525 50  0001 C CNN
F 3 "" H 8025 2525 50  0001 C CNN
	1    8025 2525
	1    0    0    -1  
$EndComp
Wire Wire Line
	8125 2075 8775 2075
Text Notes 7425 1225 0    50   ~ 0
Z =75 ohms
Text Notes 8175 2025 0    50   ~ 0
Z =50 ohms
Text Notes 2675 1175 0    50   ~ 0
Z =75 ohms
Text Notes 3275 1925 3    50   ~ 0
Z = 300 ohms
Text Notes 4675 1925 3    50   ~ 0
Z = 300 ohms
$Comp
L Connector:Conn_Coaxial J?
U 1 1 60009FF5
P 8975 2075
F 0 "J?" H 9075 2050 50  0000 L CNN
F 1 "Conn_Coaxial" H 9075 1959 50  0000 L CNN
F 2 "Connector_Coaxial:BNC_TEConnectivity_1478204_Vertical" H 8975 2075 50  0001 C CNN
F 3 " ~" H 8975 2075 50  0001 C CNN
	1    8975 2075
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60009FFB
P 8975 2275
F 0 "#PWR?" H 8975 2025 50  0001 C CNN
F 1 "GND" H 8980 2102 50  0000 C CNN
F 2 "" H 8975 2275 50  0001 C CNN
F 3 "" H 8975 2275 50  0001 C CNN
	1    8975 2275
	1    0    0    -1  
$EndComp
Text Notes 8775 1875 0    50   ~ 0
Connection to\nReceiver (RCA or BNC?)
$Comp
L Connector:Barrel_Jack J?
U 1 1 6000A002
P 2250 3900
F 0 "J?" H 2307 4225 50  0000 C CNN
F 1 "POWER" H 2307 4134 50  0000 C CNN
F 2 "" H 2300 3860 50  0001 C CNN
F 3 "~" H 2300 3860 50  0001 C CNN
	1    2250 3900
	1    0    0    -1  
$EndComp
$Comp
L _projectSymbols:DR331-105BE T?
U 1 1 6000A012
P 4500 3850
F 0 "T?" H 4900 4115 50  0000 C CNN
F 1 "4700uH" H 4900 4024 50  0000 C CNN
F 2 "DR331105BE" H 5150 3950 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/8749687" H 5150 3850 50  0001 L CNN
F 4 "Bourns DR331 Series SMD Common Mode Choke with a Ferrite Core, 1 mH Wire-Wound 500mA Idc" H 5150 3750 50  0001 L CNN "Description"
F 5 "5.3" H 5150 3650 50  0001 L CNN "Height"
F 6 "Bourns" H 5150 3550 50  0001 L CNN "Manufacturer_Name"
F 7 "DR331-105BE" H 5150 3450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-DR331-105BE" H 5150 3350 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Bourns/DR331-105BE?qs=CrTVAl0CcnKk6%252BoOsQlt3w%3D%3D" H 5150 3250 50  0001 L CNN "Mouser Price/Stock"
F 10 "8749687P" H 5150 3150 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/8749687P" H 5150 3050 50  0001 L CNN "RS Price/Stock"
F 12 "71279285" H 5150 2950 50  0001 L CNN "Allied_Number"
F 13 "https://www.alliedelec.com/bourns-dr331-105be/71279285/" H 5150 2850 50  0001 L CNN "Allied Price/Stock"
	1    4500 3850
	1    0    0    -1  
$EndComp
$Comp
L _projectSymbols:CAPACITOR_CHIP_NON-POLARIZED_EIA_0603-METRIC_1608X95 C?
U 1 1 6000A026
P 4150 3750
F 0 "C?" H 4278 3691 50  0000 L CNN
F 1 "1uF" H 4278 3600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4350 3300 50  0001 C CNN
F 3 "" H 4190 3675 50  0001 C CNN
F 4 "V?" H 4278 3509 50  0000 L CNN "Voltage"
	1    4150 3750
	1    0    0    -1  
$EndComp
Text GLabel 6600 3600 2    50   Input ~ 0
DC_PWR_IN
$Comp
L Connector:AudioJack3 J?
U 1 1 6000A046
P 2875 5625
F 0 "J?" H 2857 5950 50  0000 C CNN
F 1 "REMOTE" H 2857 5859 50  0000 C CNN
F 2 "" H 2875 5625 50  0001 C CNN
F 3 "~" H 2875 5625 50  0001 C CNN
	1    2875 5625
	1    0    0    -1  
$EndComp
Wire Wire Line
	3075 5725 3375 5725
Text GLabel 3375 5725 2    50   Input ~ 0
REMOTE_TX
Text GLabel 3375 5625 2    50   Input ~ 0
REMOTE_RX
Text GLabel 3375 5525 2    50   Input ~ 0
REMOTE_COMM
Wire Wire Line
	3375 5525 3075 5525
Wire Wire Line
	3375 5625 3075 5625
Text Notes 2975 6075 0    50   ~ 0
A9AAT-0604E\n6 conductor direct connection between boards (0.050")
Text GLabel 7000 5500 0    50   Input ~ 0
REMOTE_RX
Text GLabel 7000 5600 0    50   Input ~ 0
REMOTE_TX
Text GLabel 7500 5400 2    50   Input ~ 0
DC_PWR_IN
Text GLabel 7500 5500 2    50   Input ~ 0
PWR_DATA_OUT
$Comp
L power:GND #PWR?
U 1 1 6000A058
P 7600 5650
F 0 "#PWR?" H 7600 5400 50  0001 C CNN
F 1 "GND" H 7605 5477 50  0000 C CNN
F 2 "" H 7600 5650 50  0001 C CNN
F 3 "" H 7600 5650 50  0001 C CNN
	1    7600 5650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J?
U 1 1 6000A05E
P 7200 5500
F 0 "J?" H 7250 5817 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 7250 5726 50  0000 C CNN
F 2 "" H 7200 5500 50  0001 C CNN
F 3 "~" H 7200 5500 50  0001 C CNN
	1    7200 5500
	1    0    0    -1  
$EndComp
Text GLabel 7000 5400 0    50   Input ~ 0
REMOTE_COMM
Wire Wire Line
	7500 5600 7600 5600
Wire Wire Line
	7600 5600 7600 5650
Text Notes 6800 6000 0    50   ~ 0
CABLE TO FRONT PANEL
$Comp
L Device:Fuse F?
U 1 1 600173C1
P 2900 3600
F 0 "F?" V 2703 3600 50  0000 C CNN
F 1 "Fuse" V 2794 3600 50  0000 C CNN
F 2 "" V 2830 3600 50  0001 C CNN
F 3 "~" H 2900 3600 50  0001 C CNN
	1    2900 3600
	0    1    1    0   
$EndComp
$Comp
L Device:D_Zener D?
U 1 1 6001DAC6
P 3150 3900
F 0 "D?" V 3104 3980 50  0000 L CNN
F 1 "D_Zener" V 3195 3980 50  0000 L CNN
F 2 "" H 3150 3900 50  0001 C CNN
F 3 "~" H 3150 3900 50  0001 C CNN
	1    3150 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	2550 4000 2650 4000
$Comp
L Device:CP C?
U 1 1 600237CC
P 3650 3900
F 0 "C?" H 3768 3946 50  0000 L CNN
F 1 "CP" H 3768 3855 50  0000 L CNN
F 2 "" H 3688 3750 50  0001 C CNN
F 3 "~" H 3650 3900 50  0001 C CNN
	1    3650 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 6002D98F
P 6150 3900
F 0 "C?" H 6268 3946 50  0000 L CNN
F 1 "CP" H 6268 3855 50  0000 L CNN
F 2 "" H 6188 3750 50  0001 C CNN
F 3 "~" H 6150 3900 50  0001 C CNN
	1    6150 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6002E248
P 6150 4050
F 0 "#PWR?" H 6150 3800 50  0001 C CNN
F 1 "GND" H 6155 3877 50  0000 C CNN
F 2 "" H 6150 4050 50  0001 C CNN
F 3 "" H 6150 4050 50  0001 C CNN
	1    6150 4050
	1    0    0    -1  
$EndComp
$Comp
L _projectSymbols:CAPACITOR_CHIP_NON-POLARIZED_EIA_0603-METRIC_1608X95 C?
U 1 1 6000A03B
P 5700 3750
F 0 "C?" H 5828 3691 50  0000 L CNN
F 1 "1uF" H 5828 3600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5900 3300 50  0001 C CNN
F 3 "" H 5740 3675 50  0001 C CNN
F 4 "V?" H 5828 3509 50  0000 L CNN "Voltage"
	1    5700 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6004363E
P 5700 4050
F 0 "#PWR?" H 5700 3800 50  0001 C CNN
F 1 "GND" H 5705 3877 50  0000 C CNN
F 2 "" H 5700 4050 50  0001 C CNN
F 3 "" H 5700 4050 50  0001 C CNN
	1    5700 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 4000 2650 4200
Wire Wire Line
	2650 4200 3150 4200
Wire Wire Line
	4450 4200 4450 3950
Wire Wire Line
	4450 3950 4500 3950
Wire Wire Line
	4500 3850 4450 3850
Wire Wire Line
	4450 3850 4450 3600
Wire Wire Line
	4450 3600 4150 3600
Wire Wire Line
	2750 3600 2650 3600
Wire Wire Line
	2650 3600 2650 3800
Wire Wire Line
	2650 3800 2550 3800
Wire Wire Line
	3150 3750 3150 3600
Connection ~ 3150 3600
Wire Wire Line
	3150 3600 3050 3600
Wire Wire Line
	3150 4050 3150 4200
Connection ~ 3150 4200
Wire Wire Line
	3150 4200 3650 4200
Wire Wire Line
	3650 3750 3650 3600
Connection ~ 3650 3600
Wire Wire Line
	3650 3600 3150 3600
Wire Wire Line
	3650 4050 3650 4200
Connection ~ 3650 4200
Wire Wire Line
	3650 4200 4150 4200
Wire Wire Line
	4150 3750 4150 3600
Connection ~ 4150 3600
Wire Wire Line
	4150 3600 3650 3600
Wire Wire Line
	4150 4050 4150 4200
Connection ~ 4150 4200
Wire Wire Line
	4150 4200 4450 4200
Wire Wire Line
	5300 3850 5400 3850
Wire Wire Line
	5400 3850 5400 3600
Wire Wire Line
	5400 3600 5700 3600
Wire Wire Line
	5300 3950 5400 3950
Wire Wire Line
	5400 3950 5400 4050
$Comp
L power:GND #PWR?
U 1 1 60052D2A
P 5400 4050
F 0 "#PWR?" H 5400 3800 50  0001 C CNN
F 1 "GND" H 5405 3877 50  0000 C CNN
F 2 "" H 5400 4050 50  0001 C CNN
F 3 "" H 5400 4050 50  0001 C CNN
	1    5400 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3750 5700 3600
Connection ~ 5700 3600
Wire Wire Line
	5700 3600 6150 3600
Wire Wire Line
	6150 3750 6150 3600
Connection ~ 6150 3600
Wire Wire Line
	6150 3600 6600 3600
$EndSCHEMATC

EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 4
Title "Shared Ground Array Controller"
Date "2021-01-12"
Rev "A"
Comp "Ensemble Solutions, LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2125 1075 1750 900 
U 5FFE1B74
F0 "BackPanel" 50
F1 "file5FFE1B73.sch" 50
$EndSheet
$Sheet
S 4625 1075 1600 900 
U 5FFE2078
F0 "PowerSupplies" 50
F1 "file5FFE2077.sch" 50
$EndSheet
$Sheet
S 6875 1100 1525 875 
U 5FFE6EA4
F0 "UserInterface" 50
F1 "file5FFE6EA3.sch" 50
$EndSheet
$EndSCHEMATC
